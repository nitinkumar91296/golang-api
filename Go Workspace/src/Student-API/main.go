package main

import (
	controllers "Student-API/Controllers"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

func main() {
	r := httprouter.New()
	sc := controllers.NewStudentController(getSession())
	r.GET("/students/:id", sc.GetStudent)
	r.POST("/student", sc.CreateStudent)
	r.DELETE("/student/:id", sc.DeleteStudent)
	http.ListenAndServe("localhost:8080", r)
}
func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		fmt.Println(err)
	}
	return s
}
