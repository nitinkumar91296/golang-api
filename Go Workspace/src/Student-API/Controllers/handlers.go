package controllers

import (
	models "Student-API/Models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//StudentController to handle the mongoDB Session
type StudentController struct {
	session *mgo.Session
}

//NewStudentController it will handle all student related events
func NewStudentController(s *mgo.Session) *StudentController {
	return &StudentController{s}
}

//GetStudent displaying the student details
func (sc StudentController) GetStudent(res http.ResponseWriter, r *http.Request, p httprouter.Params) {

	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		res.WriteHeader(http.StatusNotFound) // 404
		return
	}

	oid := bson.ObjectIdHex(id)

	s := models.Student{}

	if err := sc.session.DB("student-api-db").C("student-details").FindId(oid).One(&s); err != nil {
		res.WriteHeader(404)
		return
	}

	uj, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err)
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(res, "%s\n", uj)

}

//CreateStudent inserting student into the database
func (sc StudentController) CreateStudent(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	u := models.Student{}

	json.NewDecoder(r.Body).Decode(&u)

	u.ID = bson.NewObjectId()

	sc.session.DB("student-api-db").C("student-details").Insert(u)

	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated) // 201
	fmt.Fprintf(w, "%s\n", uj)
}

//DeleteStudent delete the employee from the database using the id from db (hex value)
func (sc StudentController) DeleteStudent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(id)

	// Delete user
	if err := sc.session.DB("student-api-db").C("student-details").RemoveId(oid); err != nil {
		w.WriteHeader(404)
		return
	}

	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprint(w, "Deleted user", oid, "\n")
}
