package models

import "gopkg.in/mgo.v2/bson"

//Student struct having student details field
type Student struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	Sname  string        `json:"name" bson:"name"`
	Gender string        `json:"gender" bson:"gender"`
	Sage   int           `json:"age" bson:"age"`
	Simage string        `json:"image" bson:"image"`
}
