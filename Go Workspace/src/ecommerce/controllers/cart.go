package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"ecommerce/models"

	"gopkg.in/mgo.v2/bson"
)

var cartslice []bson.ObjectId

//GetHexFromString function that will convert the is into string
func GetHexFromString(id string) string {
	trim1 := strings.Trim(id, "ObjectIdHex")
	trim2 := strings.Trim(trim1, ")")
	trim3 := strings.Trim(trim2, "(")
	trim4 := strings.Split(trim3, "\"")
	return trim4[1]
}

//AddToCart func
func (pc ProductController) AddToCart(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			panic(err)
		}

		w.Header().Set("Content-Type", "text/html")
		productid := req.FormValue("id")
		newid := GetHexFromString(productid)
		oid := bson.ObjectIdHex(newid)
		cartslice = append(cartslice, oid)
		u := []models.Product{}
		for i := 0; i < len(cartslice); i++ {
			var datas = models.Product{}
			if err := pc.session.DB("ecommerce").C("products").FindId(cartslice[i]).One(&datas); err != nil {
				// w.WriteHeader(404)
				w.Header().Set("Content-Type", "text/html;charset=utf-8")
				fmt.Fprint(w, "No Data Found")
			}
			u = append(u, datas)
		}

		tpl.ExecuteTemplate(w, "cart.gohtml", u)
	}
}

//CheckOut func
func (pc ProductController) CheckOut(res http.ResponseWriter, req *http.Request) {

	err := tpl.ExecuteTemplate(res, "checkout.gohtml", nil)
	if err != nil {
		log.Fatal(err)
	}
}
