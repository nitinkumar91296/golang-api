package controllers

import (
	"log"
	"net/http"

	"ecommerce/models"

	"gopkg.in/mgo.v2/bson"
)

//DeleteProduct by taking a key from the user and displaying on other page
func (pc ProductController) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id") // getting id from the text field

		if !bson.IsObjectIdHex(id) {
			w.WriteHeader(http.StatusNotFound) // 404
			return
		}

		oid := bson.ObjectIdHex(id) //coverting id into bson hex id

		u := models.Product{} //u is a variable to hold the fields of the searched products

		if err := pc.session.DB("ecommerce").C("products").RemoveId(oid); err != nil {
			w.WriteHeader(404)
			return
		}
		err := tpl.ExecuteTemplate(w, "deleteproduct.gohtml", u) //executing template with the u containing all fields of the product
		if err != nil {
			log.Fatal(err)
		}
	}

}
