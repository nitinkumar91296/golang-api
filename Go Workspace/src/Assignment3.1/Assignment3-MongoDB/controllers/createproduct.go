package controllers

import (
	"Assignment3.1/Assignment3-MongoDB/models"

	"net/http"

	"gopkg.in/mgo.v2/bson"
)

//InsertProduct in the database through Postman
func (pc ProductController) InsertProduct(res http.ResponseWriter, r *http.Request) {

	// p := models.Product{}
	if r.Method == http.MethodPost {

		name := r.FormValue("name")
		price := r.FormValue("price")
		desc := r.FormValue("description")
		qty := r.FormValue("quantity")
		img := r.FormValue("image")
		p := models.Product{"", name, price, desc, qty, img}
		p.ID = bson.NewObjectId()
		pc.session.DB("ecommerce").C("products").Insert(p)
	}

	tpl.ExecuteTemplate(res, "insertproduct.gohtml", nil)

}
