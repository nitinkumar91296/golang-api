package controllers

import (
	"Assignment3.1/Assignment3-MongoDB/models"

	"net/http"

	"gopkg.in/mgo.v2/bson"
)

//InsertUpdatedProduct in the database through Postman
func (pc ProductController) InsertUpdatedProduct(res http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		id := GetHexFromString(r.FormValue("id"))
		name := r.FormValue("name")
		price := r.FormValue("price")
		desc := r.FormValue("description")
		qty := r.FormValue("quantity")
		img := r.FormValue("image")
		p := models.Product{"", name, price, desc, qty, img}
		p.ID = bson.ObjectIdHex(id)
		pc.session.DB("ecommerce").C("products").UpdateId(p.ID, p)
	}

	http.Redirect(res, r, "/", 302)

}
