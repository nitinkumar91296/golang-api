package controllers

import (
	"Assignment3.1/Assignment3-MongoDB/models"

	"log"
	"net/http"
	"text/template"

	"gopkg.in/mgo.v2"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

//ProductController contains session of mongo db
type ProductController struct {
	session *mgo.Session
}

//NewProductController returns product controller
func NewProductController(s *mgo.Session) *ProductController {
	return &ProductController{s}
}

//AllProducts display all products on the index page
func (pc ProductController) AllProducts(res http.ResponseWriter, req *http.Request) {

	var pdt = []models.Product{}
	pc.session.DB("ecommerce").C("products").Find(nil).All(&pdt) // it also returns err
	err := tpl.ExecuteTemplate(res, "index.gohtml", pdt)
	if err != nil {
		log.Fatal(err)
	}
}
