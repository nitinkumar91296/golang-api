package models

import "gopkg.in/mgo.v2/bson"

//Product fields to be stored
type Product struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Price       string        `json:"price" bson:"price"`
	Description string        `json:"description" bson:"description"`
	Quantity    string        `json:"quantity" bson:"quantity"`
	Image       string        `json:"image" bson:"image"`
}
