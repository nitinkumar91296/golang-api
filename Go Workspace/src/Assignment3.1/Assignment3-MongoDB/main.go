package main

import (
	"net/http"

	controllers "Assignment3.1/Assignment3-MongoDB/Controllers"

	"gopkg.in/mgo.v2"
)

func main() {
	pc := controllers.NewProductController(getSession())

	http.Handle("/images/", http.StripPrefix("/images", http.FileServer(http.Dir("./images"))))
	http.HandleFunc("/", pc.AllProducts)                   //index where all products are displayed
	http.HandleFunc("/createproduct", pc.InsertProduct)    //creating new products
	http.HandleFunc("/findproductdetails", pc.FindProduct) //finding a product using id
	http.HandleFunc("/deleteproduct", pc.DeleteProduct)
	http.HandleFunc("/updateproduct", pc.FetchProduct) //finding product to update
	http.HandleFunc("/insertproduct", pc.InsertUpdatedProduct)
	http.HandleFunc("/cart", pc.AddToCart)    // adding product to the cart
	http.HandleFunc("/checkout", pc.CheckOut) //Checking out to confirm the order
	http.ListenAndServe(":8080", nil)         //will run at port 8080

}

//creating a mongo db session
func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}
	return s
}
