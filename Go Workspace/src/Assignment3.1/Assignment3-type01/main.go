package main

import (
	"Assignment3/Assignment3-type01/handlers"
	"net/http"
)

func main() {

	http.HandleFunc("/", handlers.Index)
	http.HandleFunc("/products", handlers.Product)
	http.HandleFunc("/cart/", handlers.AddToCart)
	http.HandleFunc("/checkout", handlers.CheckOut)

	http.ListenAndServe(":8080", nil)
}
