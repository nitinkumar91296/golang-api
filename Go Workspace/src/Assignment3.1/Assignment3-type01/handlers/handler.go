package handlers

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

//Index page
func Index(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "index.gohtml", nil)
	if err != nil {
		log.Fatal(err)
	}
}

//Product all information displayed
func Product(res http.ResponseWriter, req *http.Request) {

	err := tpl.ExecuteTemplate(res, "products.gohtml", data)
	if err != nil {
		log.Fatal(err)
	}

}

//AddToCart func
func AddToCart(res http.ResponseWriter, req *http.Request) {
	var product Products

	for _, v := range data {
		if v.ID == req.FormValue("id") {
			product.ID = v.ID
			product.ProductName = v.ProductName
			product.Price = v.Price
			product.Description = v.Description
			product.Src = v.Src
		}

	}
	fmt.Println(product)
	err := tpl.ExecuteTemplate(res, "cart.gohtml", product)
	if err != nil {
		log.Fatal(err)
	}
}

//CheckOut func
func CheckOut(res http.ResponseWriter, req *http.Request) {

	err := tpl.ExecuteTemplate(res, "checkout.gohtml", nil)
	if err != nil {
		log.Fatal(err)
	}
}
