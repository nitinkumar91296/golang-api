package handlers

//Products ...
type Products struct {
	ID          string
	Src         string
	Price       int
	Description string
	ProductName string
}

var data = []Products{
	Products{"1", "https://image.shutterstock.com/z/stock-photo-handsome-young-bearded-man-trimming-his-beard-with-a-trimmer-762288679.jpg", 1000, "Very Good Product", "Trimmer"},
	Products{"2", "https://media.istockphoto.com/photos/luxury-watch-picture-id697173746", 2000, "Excellent Product", "Wrist Watch"},
	Products{"3", "https://images.unsplash.com/photo-1564859228273-274232fdb516?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80", 3000, "Nice Product", "T Shirt"},
	Products{"4", "https://images.unsplash.com/photo-1553062407-98eeb64c6a62?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60", 4000, "Affordable Product", "bag"},
}
