package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}

//Product ...
type Product struct {
	ID                 int    `json:"ID"`
	ProductName        string `json:"name"`
	Productdescription string `json:"designation"`
	ProductPrice       int    `json:"exp"`
	ProductImage       string `json:"salary"`
}

//ProductDetails ...
func ProductDetails(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html;charset=utf-8")
	data := []Product{
		Product{ID: 1,
			ProductName:        "Trimmer",
			Productdescription: "Philips QT4001/15 cordless rechargeable Beard Trimmer - 10 length settings",
			ProductPrice:       1199,
			ProductImage:       "https://images-na.ssl-images-amazon.com/images/I/513UZT8720L._SL1000_.jpg",
		},
		Product{ID: 2,
			ProductName:        "Hair Dryer",
			Productdescription: "Philips HP8100/46 Hair Dryer (Purple)",
			ProductPrice:       775,
			ProductImage:       "https://images-na.ssl-images-amazon.com/images/I/51FGbb3EbgL._SL1227_.jpg",
		},
		Product{ID: 3,
			ProductName:        "Straightener",
			Productdescription: "Philips BHS386 Kera Shine Straightener (Purple)",
			ProductPrice:       1955,
			ProductImage:       "https://images-na.ssl-images-amazon.com/images/I/71O9HjCXZvL._SL1500_.jpg",
		},
		Product{ID: 3,
			ProductName:        "Shampoo &  Hair Straightener",
			Productdescription: "StBotanica Pro Keratin Argan Shampoo 300ml & Conditioner 300ml Combo Pack + NOVA Hair Straightener",
			ProductPrice:       955,
			ProductImage:       "https://images-na.ssl-images-amazon.com/images/I/71KgsrTCVtL._SL1500_.jpg",
		},
	}
	// w.Write(j)

	err := tpl.ExecuteTemplate(w, "index.gohtml", data)
	if err != nil {
		log.Fatal(err)
	}
	// w.Write(j)

}
func main() {

	http.HandleFunc("/", ProductDetails)
	http.ListenAndServe(":8081", nil)
}
