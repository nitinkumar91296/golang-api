package controllers

import (
	"Library-API/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	//	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

//DeleteBook Handler to insert book into the database
func (bc BookController) DeleteBook(w http.ResponseWriter, r *http.Request) {
	b := models.GetBook{}
	json.NewDecoder(r.Body).Decode(&b)
	svc := dynamodb.New(bc.Session)
	//	av, err := dynamodbattribute.MarshalMap(b)
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"BookID": {
				N: aws.String(b.BookID),
			},
			"BookName": {
				S: aws.String(b.BookName),
			},
		},
		TableName: aws.String("Library"),
	}

	_, err := svc.DeleteItem(input)
	if err != nil {
		fmt.Println("Got error calling DeleteItem")
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Deleted item:")
	fmt.Println("BookID:  ", b.BookID)
	fmt.Println("BookName: ", b.BookName)
	fmt.Fprintf(w, "%v ", b)
}
