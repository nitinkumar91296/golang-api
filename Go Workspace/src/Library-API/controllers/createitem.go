package controllers

import (
	"Library-API/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

//BookController structure
type BookController struct {
	*session.Session
}

//NewBookController function will return book controller
func NewBookController(s *session.Session) *BookController {

	return &BookController{s}
}

//InsertBook Handler to insert book into the database
func (bc BookController) InsertBook(w http.ResponseWriter, r *http.Request) {
	b := models.Book{}
	json.NewDecoder(r.Body).Decode(&b)
	svc := dynamodb.New(bc.Session)
	av, err := dynamodbattribute.MarshalMap(b)
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("Library"),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		fmt.Println(err.Error())

	}
	fmt.Println("Created item:")
	fmt.Println("BookID:  ", b.BookID)
	fmt.Println("BookName: ", b.BookName)
	fmt.Fprintf(w, "%v ", b)

	fmt.Fprintf(w, "%v ", input)

}
