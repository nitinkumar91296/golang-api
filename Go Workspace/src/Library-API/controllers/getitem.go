package controllers

import (
	"Library-API/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	//	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

//GetBook Handler to insert book into the database
func (bc BookController) GetBook(w http.ResponseWriter, r *http.Request) {
	b := models.GetBook{}
	json.NewDecoder(r.Body).Decode(&b)
	svc := dynamodb.New(bc.Session)
	//	av, err := dynamodbattribute.MarshalMap(b)
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("Library"),
		Key: map[string]*dynamodb.AttributeValue{
			"BookID": {

				N: aws.String(b.BookID),
			},
			"BookName": {
				S: aws.String(b.BookName),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	item := models.Book{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if item.BookName == "" {
		fmt.Println("Could not find name empty")
		return
	}

	fmt.Println("Found item:")
	fmt.Println("BookID:  ", item.BookID)
	fmt.Println("BookName: ", item.BookName)
	fmt.Println("BookAuthor: ", item.BookAuthor)
	fmt.Println("BookPrice: ", item.BookPrice)

	fmt.Fprintf(w, "%v ", item)

}
