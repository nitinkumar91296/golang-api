package controllers

import (
	"Library-API/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	//	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

//UpdateBook Handler to update book into the database
func (bc *BookController) UpdateBook(w http.ResponseWriter, req *http.Request) {

	var booksupdate = models.BookUpdate{}

	err := json.NewDecoder(req.Body).Decode(&booksupdate)
	if err != nil {
		fmt.Print(err)
	}
	svc := dynamodb.New(bc.Session)

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":a": {
				S: aws.String(booksupdate.BookAuthor),
			},
			":p": {
				N: aws.String(booksupdate.BookPrice),
			},
		},
		TableName: aws.String("Library"),
		Key: map[string]*dynamodb.AttributeValue{
			"BookID": {
				N: aws.String(booksupdate.BookID),
			},
			"BookName": {
				S: aws.String(booksupdate.BookName),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set BookAuthor = :a ,BookPrice=:p "),
	}

	_, err = svc.UpdateItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Book Updated")
	fmt.Println("Book Id: ", booksupdate.BookID)
	fmt.Println("Book Name: ", booksupdate.BookName)
	fmt.Println("Book Author: ", booksupdate.BookAuthor)
	fmt.Println("Book Price: ", booksupdate.BookPrice)

	fmt.Fprintf(w, "%v \n", booksupdate)

}
