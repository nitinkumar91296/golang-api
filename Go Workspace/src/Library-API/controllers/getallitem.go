package controllers

import (
	"Library-API/models"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	//	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

//GetAllBook Handler to insert book into the database
func (bc BookController) GetAllBook(w http.ResponseWriter, r *http.Request) {
	b := models.Book{}
	json.NewDecoder(r.Body).Decode(&b)
	svc := dynamodb.New(bc.Session)
	tableName := "Library"
	proj := expression.NamesList(expression.Name("BookID"), expression.Name("BookName"), expression.Name("BookAuthor"), expression.Name("BookPrice"))
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}
	result, err := svc.Scan(params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
		os.Exit(1)
	}
 
	items := []models.Book{}
 
	for _, i := range result.Items {
		item := models.Book{}
 
		err = dynamodbattribute.UnmarshalMap(i, &item)
 
		if err != nil {
			fmt.Println("Got error unmarshalling:")
			fmt.Println(err.Error())
			os.Exit(1)
		}
		items = append(items, item)
	}
	fmt.Println(items)
	fmt.Fprintf(w,"%v",items)
}



