package models

//Book Structure
type Book struct {
	BookID     int
	BookName   string
	BookAuthor string
	BookPrice  float64
}

//GetBook structure
type GetBook struct {
	BookID   string
	BookName string
}

//BookUpdate Structure
type BookUpdate struct {
	BookID     string
	BookName   string
	BookAuthor string
	BookPrice  string
}
