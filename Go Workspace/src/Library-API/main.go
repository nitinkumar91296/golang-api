package main

import (
	"Library-API/controllers"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

func main() {
	bc := controllers.NewBookController(getSession())
	http.HandleFunc("/insert", bc.InsertBook)
	http.HandleFunc("/get", bc.GetBook)
	http.HandleFunc("/delete", bc.DeleteBook)
	http.HandleFunc("/update", bc.UpdateBook)
	http.HandleFunc("/getall", bc.GetAllBook)
	http.ListenAndServe(":8080", nil)

}

func getSession() *session.Session {

	sess, err := session.NewSession(&aws.Config{
		Endpoint: aws.String("http://localhost:8000"),
		Region:   aws.String("ap-south-1"),
		//CredentialsChainVerboseErrors: aws.Bool(true),
		//Credentials: credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	return sess
}
