package Routes

import (
	"gin-poc-UserAPI/Controllers"

	"github.com/gin-gonic/gin"
)

//SetupRouter contains the group of routes
func SetupRouter() *gin.Engine {
	r := gin.Default()
	grp := r.Group("/user-api")
	{
		grpGET("user", Controllers.GetUsers)
		grp.POST("user", Controllers.CreateUser)
		grp.GET("user/:id", Controllers.GetUserByID)
		grp.PUT("user/:id", Controllers.UpdateUser)
		grp.DELETE("user/:id", Controllers.DeleteUser)
	}
	return r
}
