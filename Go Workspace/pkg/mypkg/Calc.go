package calc

//Add addition function
func Add(args ...float64) float64 {

	sum := 0.0
	for _, i := range args {
		sum = sum + i
	}

	return sum

}

//Sub Subtraction Function
func Sub(args ...float64) float64 {

	sub := 0.0
	for _, i := range args {
		sub = i - sub
	}

	return sub

}

//Mul multiplication Function
func Mul(args ...float64) float64 {
	mul := 1.0
	for _, i := range args {
		mul = mul * i
	}
	return mul
}

//Div division Function
func Div(args ...float64) float64 {
	div := 1.0
	for _, i := range args {
		div = div * i
	}
	return div
}
